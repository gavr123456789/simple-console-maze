//#include "maze-config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void fill_maze(char maze[][9], FILE *inp);
void print_maze(/*char maze[][9]*/);
void warning();
void controll();
void winning();

/*struct player_position{
	uint x;
	uint y;
} pos;
*/


char maze[8][9]; //матрица лабиринта
const char wall   = 'x';
const char air    = 'o';
const char player = 'p';
const char ex     = 'e';
const int player_x = 1;
const int player_y = 1; 
int score = 100;

int main(void){
    FILE *mazefile;
    mazefile = fopen("maze.txt", "r");

    fill_maze(maze, mazefile); //fill the maze
	print_maze (maze);
    controll ();

    return(0);
}

//Заполняет матрицу лабиринта
void fill_maze(char maze[][9], FILE *inp){
    int x = 0;
	
    while (x < 8) {
        fscanf(inp, "%s", maze[x]);
        x++;
    }
}

void controll(){
	char user_input = '\0';
	short win = 0;
	//цикл управления
	for (int i = player_x, j = player_y; user_input!='e'; scanf("%c", &user_input), --score){
		switch (user_input) {
			case 'w':{
				//Проверка на столкновение [y][x]
				if( maze[i-1][j] != wall && maze[i-1][j] != ex){
					maze[i][j]   = air;
					maze[--i][j] = player;
					print_maze ();//TODO передавать по указателю
				}
				else if (maze[i-1][j] == ex)
					winning ();
				
				else 
					warning();	
				break;
			}
			case 's':{
				if(maze[i+1][j] != wall && maze[i+1][j] != ex){
					maze[i][j]   = air;
					maze[++i][j] = player;
					print_maze ();//TODO передавать по указателю
				}
				else if (maze[i+1][j] == ex)
					winning ();
				else 
					warning();	
				break;
			}
			case 'a':{
				if(maze[i][j-1] != wall && maze[i][j-1] != ex){
					maze[i][j]   = air;
					maze[i][--j] = player;
					print_maze ();//TODO передавать по указателю
				}
				else if (maze[i][j-1] == ex)
					winning ();						 
				else 
					warning();
						
				break;					
			}
			case 'd':{
				if(maze[i][j+1] != wall && maze[i][j+1] != ex){
					maze[i][j]   = air;
					maze[i][++j] = player;
					print_maze ();//TODO передавать по указателю
				}
				else if (maze[i][j+1] == ex)
					winning ();						
				else 
					warning();	
									
			}
				
			break;
		}
		
	}
}

//Выводит лабиринт
void print_maze(){
	//system ("clear");
	printf("\n");
	for (int x = 0; x < 8; x++) {
		printf("%s\n", maze[x]);
	}
	printf("\nscore = %d\n", score);
}

void warning(){
	printf("\n\n СТЕНА! \n\n");
	print_maze (maze);
}

void winning(){
	printf("\n\n ПОБЕДА! \n\n");
	exit (0);
}

//TODO добавить функцию которая будет находить положения игрока
// и сама его задавать


